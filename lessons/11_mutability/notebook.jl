### A Pluto.jl notebook ###
# v0.15.1

using Markdown
using InteractiveUtils

# ╔═╡ bd5290bc-6983-11eb-2f7d-2baec2ccd10e
md"""
# Изменяемые данные. Функциональный и императивный стили программирования

Ранее был рассмотрен ряд элементов, из которых строятся программы - условные вычисления, задание локальных переменных (оператор `let`), процедуры, локальные процедуры и рекурсивные вычисления. Пока что вводимые процедуры можно было рассматривать как определения для вычисления выражения на основе входных аргументов. Процедуры вводили правила преобразований, по которым одно выражение можно записать по-другому и так далее до получения конечного значения. Конечно, для конечного значения нужно иметь, в добавление к пользовательским процедурам, некоторые примитивные встроенные процедуры, которые можно рассматривать как "аксиомы".

Рассмотрим пример:
```julia
function gcd(a, b)
    let r = a % b
        r == 0 ? b : gcd(b, r)
    end
end
```

При вычислении мы можем постепенно делать замены связанных переменных на их значения, а вызовов процедур - на возвращаемые значения. Последовательность таких преобразований выражения для небольших значений аргументов можно выписать в явном виде:
```julia
1) gcd(36, 24)

2) let r = 36 % 24
       r == 0 ? 24 : gcd(24, r)
   end

3) let r = 12
       r == 0 ? 24 : gcd(24, r)
   end

4) 12 == 0 ? 24 : gcd(24, 12)

5) false ? 24 : gcd(24, 12)

6) gcd(24, 12)

...
```

Поскольку пока все вводимые конструкции использовались только для того, чтобы дать определения символам в рамках некоторой области видимости, то как только символ получил значение, его можно заменить на это значение внутри всех выражений в области видимости. Пример этого - замена `r == 0 ? 24 : gcd(24, r)` на `12 == 0 ? 24 : gcd(24, 12)`. В этом стиле программирования процедуры выражают понятие *математических функций* - получение некоторого значения, однозначно определяемого значениями входных аргументов. Стиль программирования, опирающийся преимущественно на такие процедуры, называется *функциональным*.

Хотя концепция функционального программирования весьма удобна, по существу в её рамках мы можем только доказывать некоторые "универсальные" факты. Выполнение реальных программ, с другой стороны, в большинстве случаев требует взаимодействия компьютера с окружающим миром и зачастую реакции на это взаимодействие. То есть программы так или иначе учитывать понятие времени и учитывать, что одинаковые действия по отношению к внешнему миру (например, попытка считать строку из пользовательского ввода при помощи процедуры `readline()`) могут давать разные результаты в разные моменты времени.

Эти соображения приводят нас к мысли, что выполнение программы можно рассматривать как *процесс*, который развивается во времени и в каждый момент времени может быть охарактеризован *состоянием*, которое меняется как в ходе процессов, происходящих в компьютере, так и за счёт взаимодействия с внешним миром. Продолжая эту идею, можно сказать, что вычислительные объекты также имеют состояние, которое (возможно) меняется при взаимодействии с другими вычислительными объектами.
"""

# ╔═╡ c6a40e32-6a1d-11eb-0ecd-bb534d16f172
md"""
## Изменяемые переменные состояния

Рассмотрим понятие изменяемого состояния на примере наличия внешней переменной, значение которой меняется и влияет на видимое поведение процедуры.

Предположим, нам требуется промоделировать банковский счёт, для которого определены две процедуры, `withdraw` и `deposit`, первая из которых уменьшает сумму счёте (если там достаточно средств), а вторая увеличивает. Возвращать обе процедуры должны статус операции и остаток на счёте в виде именованного кортежа.

Для поддержания информации о балансе счёта мы можем ввести переменную `balance`, внутри процедур `withdraw` и `deposit` тогда нужно изменять значение, связанное с этой переменной. Изменение значения, связанного с переменной, синтаксически не отличается от операции создания связывания: выражение `x = value` связывает имя `x` со значением `value` вне зависимости от того, имеет `x` уже какое-то значение или нет. Если `x` ещё не связан ни с каким значением, то в текущей области видимости создаётся новое связывание. Если же `x` уже связан с каким-то значением, то происходит пересвязывание (или *присваивание*) с новым значением.

Для уменьшения возможных ошибок, связанных с непреднамеренным изменением глобальных переменных, в Julia внутри процедур переменную нужно пометить ключевым словом `global`, если мы хотим изменять значение глобальной переменной с таким именем.

Определения процедур `withdraw` и `deposit` (в предположении, что снимается или добавляется положительная сумма) выглядят так:
"""

# ╔═╡ 57f494ce-6a21-11eb-01df-2357cbaf023f
begin
	balance = 100
	
	function withdraw(amount)
		global balance
		let new_balance = balance - amount
			if new_balance < 0
				(status = "Insufficient funds", balance = balance)
			else
				balance = new_balance
				(status = "Success", balance = balance)
			end
		end
	end
	
	function deposit(amount)
		global balance
		balance = balance + amount
		(status = "Success", balance = balance)
	end
end

# ╔═╡ df7e09de-6a21-11eb-07e3-e9e57e1032e5
withdraw(25)

# ╔═╡ 22a69bf4-6a22-11eb-3c37-f18b893450eb
withdraw(25)

# ╔═╡ 2854d21e-6a22-11eb-0b8e-dfaf6a425dd2
withdraw(60)

# ╔═╡ 2bd6e4ea-6a22-11eb-3fd7-2386fbd04f0c
withdraw(15)

# ╔═╡ 2fe50580-6a22-11eb-05c2-cd0a73993770
deposit(25)

# ╔═╡ 385eb4ea-6a22-11eb-3de1-799d0ac6ca4a
md"""
Процедура `withdraw` ведёт себя "неправильно" с точки зрения функционального программирования - вызовы с одним и тем же аргументом возвращают разные значения. Однако возвращаемые значения процедур `withdraw` и `deposit` соответствуют интуитивному поведению операций с банковским счётом, сумма на котором меняется после каждой произведённой операции. Говорят, что `withdraw` и `deposit` производят *побочный эффект* (кроме непосредственно возврата значения), состоящий в изменении значения переменной `balance`.

Доступность в глобальной области видимости переменной `balance`, хранящей состояние для `withdraw` и `deposit`, создаёт проблему: программист может случайно написать другие процедуры, меняющие значением этой переменной непредсказуемым образом. Можно, однако, *инкапсулировать* состояние и создать процедуры в области видимости, для которой баланс хранится в локальной переменной:
"""

# ╔═╡ 54d7c348-6a25-11eb-04b2-c57927d64f59
function make_account(initial_balance)
	let balance = initial_balance
		function withdraw(amount)
			let new_balance = balance - amount
				if new_balance < 0
					(status = "Insufficient funds", balance = balance)
				else
					balance = new_balance
					(status = "Success", balance = balance)
				end
			end
		end
		
		function deposit(amount)
			balance = balance + amount
			(status = "Success", balance = balance)
		end
		
		(withdraw = withdraw, deposit = deposit)
	end
end

# ╔═╡ 9a7c10ac-6a25-11eb-019a-8da72ea6bb38
md"""
Процедура `make_account` создаёт именованный кортеж, с полями `withdraw` и `deposit`, *хранящими процедуры*. Эти процедуры имеют внутреннее состояние, хранящееся в переменной `balance`.

Внутри процедур `withdraw` и `deposit` в данном случае можно присваивать переменной `balance` новое значение, не используя метку `global`, поскольку изменяется переменная, относящаяся к локальной области видимости внутри `let`-блока.
"""

# ╔═╡ 0d85ed52-6a26-11eb-186a-4fff4809e180
acnt1 = make_account(100)

# ╔═╡ 14e1a992-6a26-11eb-3ad2-09b817519f36
acnt2 = make_account(100)

# ╔═╡ 25b7d0d4-6a26-11eb-2528-39d09d49ef37
acnt1.withdraw(10)

# ╔═╡ 2c7df260-6a26-11eb-3013-e99aa4984cff
acnt2.withdraw(50)

# ╔═╡ 31bdf050-6a26-11eb-2756-6f591d4931d2
acnt1.withdraw(200)

# ╔═╡ a2c87e5c-6a26-11eb-09fe-c570bf897195
md"""
В этой реализации переменные `balance` являются локальными для объектов, создаваемых через `make_account`. У процедур, не входящих в состав такого объекта, нет доступа к локальному значению `balance`. Работа с локальной для объекта переменной, таким образом, может теперь осуществляться только через процедуры `withdraw` и `deposit`, входящие в состав этого объекта.

Такие процедуры-элементы структуры, которые могут изменять некое локальное состояние, - основа "классического" объектно-ориентированного стиля программирования. В Julia такой ООП-стиль не идиоматичен, вместо него принято использовать процедуры, работающие с изменяемыми данными.

Стиль программирования, в котором широко используются операции, изменяющие значения переменных, называется *императивным*. Программы в императивном стиле нужно понимать как запись последовательности *действий*, которые должны выполняться в записанном порядке для получения конечного результата.
"""

# ╔═╡ 6f6b0338-6ace-11eb-289f-61ee2f405127
md"""
**Упражнение:** Напишите процедуру для создания банковского счёта, защищённого паролем. Процедуры `withdraw` и `deposit`, кроме суммы, должны принимать строку-пароль. Если пароль совпадает с заданным при создании счёта, то операция выполняется, в противном случае процедура выдаёт `(status = "Incorrect password", balance = 0)`.
```julia
julia> acnt = make_passwd_account(100, "secret_password")

julia> acnt.withdraw(40, "secret_password")
(status = "Success", balance = 60)

julia> acnt.deposit(50, "other_password")
(status = "Incorrect password", balance = 0)
```
"""

# ╔═╡ f74cbba4-6acf-11eb-05c8-8bbd600fe762


# ╔═╡ f7ca6de4-6acf-11eb-075e-89f5a07cecfa
md"""
**Упражнение:** Дополните предыдущее определение так, чтобы, если неверный пароль вводится больше семи раз подряд, счёт блокировался и при любой попытке операции возвращал `(status = "Account blocked", balance = 0)`
"""

# ╔═╡ 690a642a-6ad0-11eb-16ec-3d0a064294c7


# ╔═╡ 2c2db774-6a28-11eb-39f5-2926dc1a1ff0
md"""
## Рекурсия и циклическое выполнение

В функциональном программировании для описания повторяющихся вычислений используются рекуррентные процедуры. В некоторых случаях рекуррентные процедуры могут быть переписаны в ином виде, если разрешено изменять значения локальных переменных. Рассмотрим снова процедуру вычисления наибольшего общего делителя:
```julia
function gcd(a, b)
    b == 0 ? a : gcd(b, a % b)
end
```

Как видим, если `b` равно нулю, то нужно вернуть `a`, иначе заменить значение `a` на `b`, а `b` на остаток деления (исходного) `a` на `b` и повторить процесс.

Такая конструкция, где проверяется условие, при его выполнении производятся некие действия, затем снова проверяется условие, при его выполнении опять выполняются такие же действия и т.д. пока условие не перестанет выполняться, описывается циклом `while` ("пока"):
```julia
while ⟨condition⟩
    ⟨computations⟩
end
```
То есть `gcd` можно переписать через цикл:
"""

# ╔═╡ 20a23a0e-6a2a-11eb-0fa2-31c2602f8cc1
function gcd(a::Integer, b::Integer)
	while b != 0
		r = a % b
		a = b
		b = r
	end
	return a
end

# ╔═╡ 7e70a65c-6a2a-11eb-0d61-6f58701fa094
gcd(36, 24)

# ╔═╡ 8ef150da-6a2a-11eb-1b86-2dceb9ef46f7
md"""
Внутри тела цикла при каждом повторе (повтор также называют "итерация") создаётся локальная область видимости. Это значит, что в данном случае на каждой итерации цикла создаётся новая локальная переменная `r`, которая видна только в теле цикла.

Как видно, присваивания создают скользкий момент - поскольку после присваивания значение, связанное с именем переменной, изменяется, нельзя записать
```julia
while b != 0
    a = b
    b = a % b
end
```
т.к. после присваивания `a = b` выражение `b = a % b` эквивалентно `b = b % b`, т.е. `b = 0`. Также нельзя записать и
```julia
while b != 0
    b = a % b
    a = b
end
```
поскольку при этом теряется исходное значение `b`. В демонстрационных целях в определении написан правильный ход присваиваний с введением дополнительной переменной. Более идиоматическим (и, пожалуй, более читаемым) способом будет использование кортежного присваивания
```julia
while b != 0
    a, b = b, a % b
end
```
В такой записи подчёркивается, что переменные `a` и `b` составляют *состояние*, которое должно меняться как единое целое. При кортежном присваивании сначала вычисляется кортеж (или другой объект, для которого определена итерация), записанный справа, и только затем происходит присваивание элементов кортежа переменным, стоящим слева.
"""

# ╔═╡ Cell order:
# ╟─bd5290bc-6983-11eb-2f7d-2baec2ccd10e
# ╟─c6a40e32-6a1d-11eb-0ecd-bb534d16f172
# ╠═57f494ce-6a21-11eb-01df-2357cbaf023f
# ╠═df7e09de-6a21-11eb-07e3-e9e57e1032e5
# ╠═22a69bf4-6a22-11eb-3c37-f18b893450eb
# ╠═2854d21e-6a22-11eb-0b8e-dfaf6a425dd2
# ╠═2bd6e4ea-6a22-11eb-3fd7-2386fbd04f0c
# ╠═2fe50580-6a22-11eb-05c2-cd0a73993770
# ╟─385eb4ea-6a22-11eb-3de1-799d0ac6ca4a
# ╠═54d7c348-6a25-11eb-04b2-c57927d64f59
# ╟─9a7c10ac-6a25-11eb-019a-8da72ea6bb38
# ╠═0d85ed52-6a26-11eb-186a-4fff4809e180
# ╠═14e1a992-6a26-11eb-3ad2-09b817519f36
# ╠═25b7d0d4-6a26-11eb-2528-39d09d49ef37
# ╠═2c7df260-6a26-11eb-3013-e99aa4984cff
# ╠═31bdf050-6a26-11eb-2756-6f591d4931d2
# ╟─a2c87e5c-6a26-11eb-09fe-c570bf897195
# ╟─6f6b0338-6ace-11eb-289f-61ee2f405127
# ╠═f74cbba4-6acf-11eb-05c8-8bbd600fe762
# ╟─f7ca6de4-6acf-11eb-075e-89f5a07cecfa
# ╠═690a642a-6ad0-11eb-16ec-3d0a064294c7
# ╟─2c2db774-6a28-11eb-39f5-2926dc1a1ff0
# ╠═20a23a0e-6a2a-11eb-0fa2-31c2602f8cc1
# ╠═7e70a65c-6a2a-11eb-0d61-6f58701fa094
# ╟─8ef150da-6a2a-11eb-1b86-2dceb9ef46f7
